
const logout=document.querySelector("#logout");
logout.addEventListener('click',(e)=>{
    e.preventDefault();
    auth.signOut();
});
firebase.auth().onAuthStateChanged(function(user) {
    
    if(user==null)
    {
        window.location.href="/index.html"
    }
    else
    {
    }
});

const uploadPresetForm=document.querySelector('#uploadPreset-form');
uploadPresetForm.addEventListener('submit',(e)=>{
    e.preventDefault();
    var user = firebase.auth().currentUser;
    const title=uploadPresetForm['uploadPreset-title'].value;
    const description=uploadPresetForm['uploadPreset-description'].value;
    uname=user.displayName.split("@")[1]
    const imgfiles=uploadPresetForm['uploadPreset-img']
    var storageRef = firebase.storage().ref('presets/'+uname+"/"+title+"/"+"previewImg/previewimg");
    var task = storageRef.put(imgfiles.files[0]);
    storeSlidePic(title)
    const xmpfile=uploadPresetForm['uploadPreset-xmp']
    var storageRef = firebase.storage().ref('presets/'+uname+"/"+title+"/"+"xmp/"+xmpfile.files[0].name);
    var task = storageRef.put(xmpfile.files[0]);
    const dmgfile=uploadPresetForm['uploadPreset-dmg']
    var storageRef = firebase.storage().ref('presets/'+uname+"/"+title+"/"+"dmg/"+dmgfile.files[0].name);
    var task = storageRef.put(dmgfile.files[0]);
    addPresetInfo()


})
function writePresetData(userId, name, email, imageUrl) {
    firebase.database().ref('preset/' + userId).set({
      username: name,
      email: email,
      profile_picture : imageUrl
    });
  }
function checkIfEmpty(input){
   
    document.getElementById("previewPic").src="img/no-image.png"
    
}
  function LoadPreviewPic(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            document.getElementById("previewPic").src=e.target.result;
        };

        reader.readAsDataURL(input.files[0]);
    }
}
function LoadPicSlider(input) {
    if (input.files && input.files[0]) {
   
        var result=input.id[0]+"p"+input.id[1];

        var reader = new FileReader();

        reader.onload = function (e) {
            document.getElementById(result).src=e.target.result;
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function addPresetInfo()
{
    var user = firebase.auth().currentUser;
    uname=user.displayName.split("@")[1]
    const uploadPresetForm=document.querySelector('#uploadPreset-form');
    const title=uploadPresetForm['uploadPreset-title'].value;
    const description=uploadPresetForm['uploadPreset-description'].value;
    //MOOOD
    const happy=document.getElementById("happy").checked;
    const dreamy=document.getElementById("dreamy").checked;
    const epic=document.getElementById("epic").checked;
    const sad=document.getElementById("sad").checked;
    const mysterious=document.getElementById("mysterious").checked;
    const euphoric=document.getElementById("euphoric").checked;
    //GENER
    const food=document.getElementById("food").checked;
    const cinematic=document.getElementById("cinematic").checked;
    const portrait=document.getElementById("portrait").checked;
    const cars=document.getElementById("cars").checked;
    const sport=document.getElementById("sport").checked;
    const Animals=document.getElementById("Animals").checked;
    const street=document.getElementById("street").checked;

    const date=new Date()

    db.collection("presetInfo").doc(uname).collection("presets").doc(title).set({
        date: date,
        title: title,
        description:description,
        happy:happy,
        dreamy:dreamy,
        epic:epic,
        sad:sad,
        mysterious:mysterious,
        euphoric:euphoric,
        food:food,
        cinematic:cinematic,
        portrait:portrait,
        cars:cars,
        sport:sport,
        Animals:Animals,
        street:street,
        numOfStars:0,
        numOfReviews:0


    })
    .then(function() {
        console.log();
    })
    .catch(function(error) {
        console.error("Error writing document: ", error);
    });
    

    
}

function storeSlidePic(title)
{
    
    var counter=0;
    var user = firebase.auth().currentUser;
    uname=user.displayName.split("@")[1]
    const uploadPresetForm=document.querySelector('#uploadPreset-form');
    const b1=uploadPresetForm['b1']
    const a1=uploadPresetForm['a1']
    const b2=uploadPresetForm['b2']
    const a2=uploadPresetForm['a2']
    const b3=uploadPresetForm['a3']
    const a3=uploadPresetForm['b3']
    const b4=uploadPresetForm['b4']
    const a4=uploadPresetForm['a4']
    if(a1.files.length!=0&&b1.files.length!=0)
    {
        counter+=1;
        var storageRef = firebase.storage().ref('presets/'+uname+"/"+title+"/"+"before/b"+counter);
        var task = storageRef.put(b1.files[0]);
        var storageRef = firebase.storage().ref('presets/'+uname+"/"+title+"/"+"after/a"+counter);
        var task = storageRef.put(a1.files[0]);

        
    }
    if(a2.files.length!=0&&b2.files.length!=0)
    {
        counter+=1;
        var storageRef = firebase.storage().ref('presets/'+uname+"/"+title+"/"+"before/b"+counter);
        var task = storageRef.put(a2.files[0]);
        var storageRef = firebase.storage().ref('presets/'+uname+"/"+title+"/"+"after/a"+counter);
        var task = storageRef.put(b2.files[0]);

    }
    if(a3.files.length!=0&&b3.files.length!=0)
    {
        counter+=1;
        var storageRef = firebase.storage().ref('presets/'+uname+"/"+title+"/"+"before/b"+counter);
        var task = storageRef.put(a3.files[0]);
        var storageRef = firebase.storage().ref('presets/'+uname+"/"+title+"/"+"after/a"+counter);
        var task = storageRef.put(b3.files[0]);

    }
    if(a4.files.length!=0&&b4.files.length!=0)
    {
        counter+=1;
        var storageRef = firebase.storage().ref('presets/'+uname+"/"+title+"/"+"before/b"+counter);
        var task = storageRef.put(a4.files[0]);
        var storageRef = firebase.storage().ref('presets/'+uname+"/"+title+"/"+"after/a"+counter);
        var task = storageRef.put(b4.files[0]);

    }
}






