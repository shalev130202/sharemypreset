
//check if auth status changes
auth.onAuthStateChanged(user=>{
     if(user){
         console.log(user)
         setupUI(user)
     }
     else
     {
        setupUI()
         console.log("user logged out")
     }
})
//signup
var flag=0
const signupForm=document.querySelector('#signup-form');
//signup user
signupForm.addEventListener('submit',(e)=>{
    e.preventDefault();
    //get user info
    const email=signupForm['signup-email'].value;
    const name=signupForm['signup-name'].value;
    const password=signupForm['signup-password'].value;
    const bio=signupForm['signup-bio'].value;
    const userName=signupForm['signup-UserName'].value;
    checkUsername(userName).then(function(result){
        if(result)
        {
            alert("please change your user name")
            return true
        }
            

    }).then((result)=>{
        if(!result)
        {    //signup user

            auth.createUserWithEmailAndPassword(email,password).then(cred=>{
        
                var user = firebase.auth().currentUser;
                const backgroundpic=signupForm['signup-bgrImg']
                const profilepic=signupForm['signup-profileImg']
                var storageRef = firebase.storage().ref('users/profileImg/'+userName);
                
        
                if(profilepic.files[0]!=null)
                {
                    task = storageRef.put(profilepic.files[0]).then(()=>{
                        storageRef.getDownloadURL().then(function(url) {
                            console.log("profile img:",url)
                            user.updateProfile({
                                displayName :name+"@"+userName,
                                photoURL:url
                                });
                            
                        });
                    }).then(()=>{
                        var storageRef = firebase.storage().ref('users/backgroundImg/'+userName);
                        if(backgroundpic.files[0]!=null)
                        {
                            storageRef.put(backgroundpic.files[0])
                        }
                        if (bio==null)
                            var database = db.collection("userInfo").doc(userName).set({uname:userName,bio:"set your profile bio"});
                        else
                            var database = db.collection("userInfo").doc(userName).set({uname:userName,bio:bio});
                
                        
                        
                        const modal=document.querySelector('#modal-signup');
                        M.Modal.getInstance(modal).close();
                            
                        signupForm.reset();
                    });
                }
                else
                {
                    user.updateProfile({
                        displayName :name+"@"+userName
                        
                    });    
        
                    var storageRef = firebase.storage().ref('users/backgroundImg/'+userName);
                    if(backgroundpic.files[0]!=null)
                    {
                        storageRef.put(backgroundpic.files[0])
                    }
                    if (bio==null)
                        var database = db.collection("userInfo").doc(userName).set({uname:userName,bio:"set your profile bio"});
                    else
                        var database = db.collection("userInfo").doc(userName).set({uname:userName,bio:bio});
            
                    
                    
                    const modal=document.querySelector('#modal-signup');
                    M.Modal.getInstance(modal).close();
                        
                    signupForm.reset();        
                }
               
                
                
                
              
                
                
            }).catch(function(error) {
                
                var errorMessage = error.message;
                alert(errorMessage)
                
                });
                
                flag=0
        
        }
    });
        
   
        
    
});
  
//logout
const logout=document.querySelector("#logout");
logout.addEventListener('click',(e)=>{
    console.log( localStorage.getItem('userName'))
    e.preventDefault();
    auth.signOut();
});
//login

const loginForm=document.querySelector('#login-form');
loginForm.addEventListener('submit',(e)=>{
    e.preventDefault();
    const email=loginForm['login-email'].value;
    const password=loginForm['login-password'].value;
    auth.signInWithEmailAndPassword(email,password).then((cred)=>{

        const modal=document.querySelector('#modal-login');
        M.Modal.getInstance(modal).close();
        loginForm.reset();
    }).catch(function(error) {
       
        var errorMessage = error.message;
        alert(errorMessage)
        
      });
})
//google signup
const googlesignIn=document.querySelector("#googleSignUp");

googlesignIn.addEventListener('click',(e)=>{

    e.preventDefault();
    var provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithPopup(provider).then(function(result) {
        var user = firebase.auth().currentUser;
        if(result.additionalUserInfo.isNewUser)
        {
            
            flag=false
            username=makeid();
            checkUsername(username).then(function(result){
                while(!flag)
                {
                    if(!result)
                    {
                        
                        
                        
                        db.collection("userInfo").doc(username).set({uname:username,bio:"set your profile bio"}).then(()=>{
                           
                            const modal=document.querySelector('#modal-signup');
                            M.Modal.getInstance(modal).close();
                            signupForm.reset();
                        });
                        flag=true
                        
                        
                    }
                    else
                    {
                        username=makeid();
                    }
                    
                }
                
            });
            //check if user name exsit
           
        }
        else
        {
            const modal=document.querySelector('#modal-signup');
            M.Modal.getInstance(modal).close();
            signupForm.reset();
        }
        user.updateProfile({
            displayName :name+"@"+username
            
        });    
    
    });
});

//google login
const googlelogin=document.querySelector("#googlelogin");

googlelogin.addEventListener('click',(e)=>{
    e.preventDefault();
    var provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithPopup(provider).then(function(result) {
        var user = firebase.auth().currentUser;

        if(result.additionalUserInfo.isNewUser)
        {
            
            flag=false
            username=makeid();
            checkUsername(username).then(function(result){
                while(!flag)
                {
                    if(!result)
                    {
                        
                        
                        
                        db.collection("userInfo").doc(username).set({uname:username,bio:"set your profile bio"}).then(()=>{
                           
                            const modal=document.querySelector('#modal-login');
                            M.Modal.getInstance(modal).close();
                            signupForm.reset();
                        });
                        flag=true
                        
                        
                    }
                    else
                    {
                        username=makeid();
                    }
                    
                }
                
            });
            //check if user name exsit
           
        }
        else
        {
            const modal=document.querySelector('#modal-login');
            M.Modal.getInstance(modal).close();
            signupForm.reset();
        }
        user.updateProfile({
            displayName :name+"@"+username
            
        });  
      });
});
function makeid() {
    var length=25
    var result           = '';
    var characters       = 'ABCDEFGHIJK!@#$%^&*LMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}
function isThereUser(username){
    users=firebase.database().ref("users");
    users.once("value")
    .then(function(snapshot) {
        
        if (snapshot.hasChild(username)) {
            alert("false")
            return false
        } else {
           
        }
    });
     alert("true")
    return true
}
function checkUsername(username)
{
    const ref = db.collection(`userInfo`).doc(username);

    
    return (ref.get()
    .then(doc => {
      return doc.exists
      
    }))
}
function temp()
{
    const userName=signupForm['signup-UserName'].value
    checkUsername(userName).then(function(result){
        if(result)
        {
            document.getElementById("signup-UserName").style.backgroundColor = "crimson"
            
        }
        else
        {
            document.getElementById("signup-UserName").style.backgroundColor = "lightgreen"
         
        }
    });
    
    
}