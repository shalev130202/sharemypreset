
document.addEventListener('DOMContentLoaded', function() {

    var modals = document.querySelectorAll('.modal');
    M.Modal.init(modals);
  
  });
  
const loggedOutLinks=document.querySelectorAll('.logged-out');
const loggedInLinks=document.querySelectorAll('.logged-in');
const setupUI=(user)=>{
    if (user){
        //toggle Ui elements
        loggedInLinks.forEach(item=>item.style.display='block')
        loggedOutLinks.forEach(item=>item.style.display='none')
    }
    else{
        //toggle Ui elements
        loggedInLinks.forEach(item=>item.style.display='none')
        loggedOutLinks.forEach(item=>item.style.display='block')

    }
}


var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
}
window.addEventListener('load', function () {
  const urlParams = new URLSearchParams(window.location.search);
  const uname = urlParams.get('uname');
  const title = urlParams.get('preset').replace("_"," ");
  db.collection("presetInfo").doc(uname).collection("presets").doc(title).get().then(snapshot => {
    
      data=snapshot.data()
      document.getElementById("title").innerHTML=data["title"]
      document.getElementById("description").innerHTML=data["description"]
      
     
  });
  db.collection("userInfo").doc(uname).get().then(snapshot => {
    data=snapshot.data()
    document.getElementById("uname").innerHTML=uname

    document.getElementById("bio").innerHTML=data["bio"]
      
  });
  var storageRef = firebase.storage().ref('users/profileImg/'+uname);
  storageRef.getDownloadURL().then(function(url) {
    document.getElementById("profileImg").src=url
      
  });
  var storageRef = firebase.storage().ref('presets/'+uname+"/"+title+"/previewImg/previewimg");
  storageRef.getDownloadURL().then(function(url) {
    document.getElementById("previewImg").src=url
      
    
  });
  var storageRef = firebase.storage().ref('presets/'+uname+"/"+title+"/before");
  storageRef.listAll().then(function(result) {
    for (i=2;i<result.items.length;i++)
    {
      document.getElementById("slider").innerHTML+=`
      <div class="mySlides fade">
     
      <div class="ba-Slider" style="width:852px;height:480px;" unselectable='on' onselectstart='return false;' onmousedown='return false;'>    
        <div id="before"><img id="b`+i+`" /></div>

        <div class="slider" style="width:32px !important; height:32px !important;"></div>
      
        <div id="after"><img id="a`+i+`" /></div>
      </div>
      
    </div>
      `
      document.getElementById("dots").innerHTML+=`<span class="dot" onclick="currentSlide(`+i+`)"></span>`
    }
    var i=2
    result.items.forEach(function(imageRef) {
      imageRef.getDownloadURL().then(function(url) {
          document.getElementById("b"+i).src=url
      })
      
      i+=1

    });
  })
  var storageRef = firebase.storage().ref('presets/'+uname+"/"+title+"/after");
  storageRef.listAll().then(function(result) {
    var i=2
    result.items.forEach(function(imageRef) {
      imageRef.getDownloadURL().then(function(url) {
          document.getElementById("a"+i).src=url
      })
      
      i+=1

    });
  })

})







var moveSlider = false;
$(document).ready(function(){
    $(".ba-Slider").each(function(i){
        $(this).children(".slider").mousedown(function(){
             moveSlider = true;
            $(this).parent().children("#before").removeClass("ease");
            $(this).removeClass("ease");
        });
        $(this).children(".slider").mouseup(function(){
            moveSlider = false;
            $(this).parent().children("#before").addClass("ease");
            $(this).addClass("ease");
            var minmax = $(this).parent().width() / 8;
            if($(this).parent().children("#before").width() > $(this).parent().width() - minmax){
                $(this).parent().children("#before").width("100%");
                var sOffset = $(this).parent().width() - 16.5;
                $(this).css("left", sOffset);
            }else if($(this).parent().children("#before").width() < minmax){
                $(this).parent().children("#before").width(0);
                var sOffset = -16.5;
                $(this).css("left", sOffset);
             }
            
        });
        
        $(this).mouseup(function(){
            moveSlider = false;
            $(this).children("#before").addClass("ease");
            $(this).children(".slider").addClass("ease");
            var minmax = $(this).width() / 8;
            if($(this).children("#before").width() > $(this).width() - minmax){
                $(this).children("#before").width("100%");
                var sOffset = $(this).width() - 16.5;
                $(this).children(".slider").css("left", sOffset);
            }else if($(this).children("#before").width() < minmax){
                $(this).children("#before").width(0);
                var sOffset = -16.5;
                $(this).children(".slider").css("left", sOffset);
             }
            
            
        });
        $(this).mousemove(function(e){
            if(moveSlider == true){
                var pOffset = $(this).offset(); 
                var mouseX = e.pageX - pOffset.left;
                $(this).children("#before").width(mouseX - 0.5);
                var sOffset = mouseX - 16.5;
                $(this).children(".slider").css("left", sOffset);
            }
            
        });
    });
});
