
document.addEventListener('DOMContentLoaded', function() {

    var modals = document.querySelectorAll('.modal');
    M.Modal.init(modals);
  
  });
  const logout=document.querySelector("#logout");
logout.addEventListener('click',(e)=>{
    e.preventDefault();
    auth.signOut();
});
firebase.auth().onAuthStateChanged(function(user) {
    uname=user.displayName.split("@")[1]
    if(user==null)
    {
        window.location.href="/index.html"
    }
    if (user.photoURL==null || user.photoURL.startsWith("https://lh3.googleusercontent.com"))
        document.getElementById("profilepic").src="/img/no-image.png"
    else
        document.getElementById("profilepic").src=user.photoURL
    var storageRef = firebase.storage().ref('users/backgroundImg/'+uname);
    storageRef.getDownloadURL().then(function(url) {
                    
        document.getElementById("bannerpic").src=url
        
    }).catch(function(error){
        document.getElementById("bannerpic").src="img/no-image-box.png"
    });
});


function resetpass() { 
    var user = firebase.auth().currentUser;
    auth.sendPasswordResetEmail(user.email).then(function() {
        alert("Email sent")
      }).catch(function(error) {
            alert(error.messege)
      });
}



 //upadate profile form
const editprofileForm=document.querySelector('#editprofile-form');


editprofileForm.addEventListener('submit',(e)=>{
    e.preventDefault();
    bannerpic=editprofileForm["bannerpic"]
    profilepic=editprofileForm["profileimg"]
    var user = firebase.auth().currentUser;
    uname=user.displayName.split("@")[1]
    if( editprofileForm["bio"].value!="")
    {
       
        
        
        db.collection("userInfo").doc(uname).update({
            "bio":editprofileForm["bio"].value 
        })
        .then(function() {
            
        }).catch(function(error){console.log(error)});
     
    }
    if(editprofileForm["name"].value!="")
    {
        user.updateProfile({
            displayName: editprofileForm["name"].value+"@"+uname
            
          }).then(function(){
            
          })
    }
    var storageRefprofile = firebase.storage().ref('users/profileImg/'+uname);
    if(profilepic.files[0]!=null)
    {
        
        var task = storageRefprofile.put(profilepic.files[0]).then(()=>{
            storageRefprofile.getDownloadURL().then(function(url) {
                console.log("profile image:",url)
                user.updateProfile({
                    
                    photoURL:url
                  }).then(function(){});
                

            });
        }).catch(function(error){
            console.log(error)
        });
    }
    var storageRefbgr = firebase.storage().ref('users/backgroundImg/'+uname);
    if(bannerpic.files[0]!=null)
    {
        var task = storageRefbgr.put(bannerpic.files[0]).then(()=>{
           
        });
    }
    
    
});

function LoadProfilePic(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            document.getElementById("profilepic").src=e.target.result;
        };

        reader.readAsDataURL(input.files[0]);
    }
}


function LoadBannerPic(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            document.getElementById("bannerpic").src=e.target.result;
        };

        reader.readAsDataURL(input.files[0]);
    }
}