var finishLoadFiles=false
const logout=document.querySelector("#logout");
logout.addEventListener('click',(e)=>{
    e.preventDefault();
    auth.signOut();
});

firebase.auth().onAuthStateChanged(function(user) {
    
    if(user==null)
    {
        window.location.href="/index.html"
    }
    else
    {
        getUserPresets()
        name=user.displayName.split("@")[0]
        uname=user.displayName.split("@")[1]
        db.collection('userInfo').doc(uname).get().then(snapshot=>{
            document.getElementById("bio").innerHTML="<p style='margin-block-start:0px !important; margin-block-end:0px !important '>"+snapshot.data().bio+"</p>"+"<div style='color: gray'>@"+uname+"</div>";
        })
        if (user.photoURL==null || user.photoURL.startsWith("https://lh3.googleusercontent.com"))
            document.getElementById("profilepic").src="https://images.pexels.com/photos/1804796/pexels-photo-1804796.jpeg?auto=compress&amp;cs=tinysrgb&amp;dpr=2&amp;h=750&amp;w=1260"
        else
            document.getElementById("profilepic").src=user.photoURL
            console.log(user.photoURL)
        var storageRef = firebase.storage().ref('users/backgroundImg/'+uname);
        storageRef.getDownloadURL().then(function(url) {
                    document.getElementById("banner").style.backgroundImage ="url('"+url+"')"
                    finishLoadFiles=true
                    const loader = document.querySelector(".loader");
                    loader.className += " hidden"; // class "loader hidden"
                    
           
            
        });
        
        name="<h4 style=' margin-block-end:0px !important '>"+name+"</h4>"
        console.log(fname)
        document.getElementById("fname").innerHTML=name;

       
       
        

    }
   
  });

  $(document).ready(function() {


});
function addItem(numOfStars,numofReviews,title,previewImgUrl,username,profilepic) {

black=5-numOfStars
numOfStars=0;//remvove
stars=""
if(numOfStars==0)
{
    stars+=`<span class="fa fa-star"></span>
    <span class="fa fa-star"></span>
    <span class="fa fa-star"></span>
    <span class="fa fa-star"></span>
    <span class="fa fa-star"></span>`
}
else
{
    for(i=0;i<numOfStars;i++)
    {
        stars+=`<span class="fa fa-star checked"></span>`
    }
    for(i=0;i<black;i++)
    {
        stars+=`<span class="fa fa-star"></span>`
    }
}

$('.owl-carousel').trigger('add.owl.carousel', [`<div class="item" onclick="location.href='preset.html?uname=`+username+`&preset=`+title.replace(" ","_")+`';" style=" overflow:auto; word-wrap: break-word; border-style: solid;">
        
<img style="margin-bottom: -13px;"  src="`+previewImgUrl+`">
<div style="margin-right: 13px; margin-left: 13px;">
<h5>`+title+`</h5>`+stars+`
</div>
<div class="line"></div>
<div class="middle">
    <img src="`+profilepic+`" class="avatar"> <div style=" font-size: large; display: inline-block;"> <a href="/index.html" >`+username+`</a></div> 
</div>
</div>`]).trigger('refresh.owl.carousel');

}



function getUserPresets()
{
    var user = firebase.auth().currentUser;
    uname=user.displayName.split("@")[1]
    db.collection("presetInfo").doc(uname).collection("presets").get().then(snapshot => {
        snapshot.forEach(doc => {
            preset=doc.data()
            stars=preset["numOfReviews"]/preset["numOfStars"]
            var storageRef = firebase.storage().ref('presets/'+uname+"/"+preset["title"]+"/previewImg/previewimg");
            storageRef.getDownloadURL().then(function(url) {
                addItem(stars,preset["numOfReviews"],preset["title"],url,uname,user.photoURL)
            });
            
            console.log(doc.data());
        });
    })
}